import {
  Component,
  OnInit,
  ElementRef
} from '@angular/core';
//import { formatDate } from '@angular/common';
// tslint:disable-next-line: no-duplicate-imports
import { ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppConfigService, AuthenticationService, NodesApiService } from '@alfresco/adf-core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RowFilter, ShareDataRow } from '@alfresco/adf-content-services';
import { CreateFolderComponent } from '../create-folder/create-folder.component';
import { Node } from '@alfresco/js-api';

@Component({
  selector: 'aca-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {
  
  PARENT_NODE: string;
  LOCALHOST: string = this.appConfig.getLocationHostname();
  URL_API: string;

  isLinear = true;
  docFormGroup: FormGroup;
  docUploadGroup: FormGroup;
  currentType: string;
  fileToUpload: File = null;
  uploadNextDis = true;
  uploadContentExist = false;
  uploadCompleted = false;
  spinnerOn = false;
  validationDis = false;
  yearFolderExist = false;
  monthFolderExist = false;
  date = new Date();
  folderFilter: RowFilter;
  newfolder: CreateFolderComponent;
  children = [];
 
  @ViewChild('fileDropRef')
  myInputVariable: ElementRef;

  constructor(
    private http: HttpClient,
    private auth: AuthenticationService,
    private appConfig: AppConfigService,
    //private alfrescoJsApi: AlfrescoApi,
    private nodesApi: NodesApiService
  ) {
    console.log("url : "+this.URL_API);

      this.folderFilter = (row: ShareDataRow) => {
        let node = row.node.entry;
        
        if (node && node.isFolder && node.name=="testCpl") { //Possibilité d'ajouter plus de critère : 
            return true;
        }
        
        return false;
    };
  }

  ngOnInit() {
    this.uploadNextDis = true;
    this.uploadContentExist = false;
    this.currentType = 'Contrat';
    this.uploadCompleted = false;
    this.spinnerOn = false;
    this.fileToUpload = null;

    this.docFormGroup = new FormGroup({
      nameDoc: new FormControl('', Validators.required),
      yearDoc: new FormControl(this.date.getFullYear(), Validators.required),
      monthDoc: new FormControl(this.date.getMonth()+1, Validators.required)
    });

    this.docUploadGroup = new FormGroup({
      docUpload: new FormControl('')
    });
  }

  fileBrowseHandler(event: Event) {
    const element = event.target as HTMLInputElement;
    this.fileToUpload = element.files.item(0);
    this.uploadNextDis =  false;
    this.uploadContentExist = true;
    this.uploadCompleted = true;
  }

  resetDocumentUpload() {
    this.uploadNextDis = true;
    this.uploadContentExist = false;
    this.uploadCompleted = false;
    this.fileToUpload = null;
    this.myInputVariable.nativeElement.value = '';
  }

  valider() {

    let formData: FormData = new FormData();
    formData.append('filedata', this.fileToUpload, this.fileToUpload.name);
    formData.append('nodeType', 'cm:content');
    formData.append('year', this.docFormGroup.get('yearDoc').value);
    formData.append('month', this.docFormGroup.get('monthDoc').value);
    this.docFormGroup.patchValue({nameDoc:this.fileToUpload.name});

    this.uploadNextDis = true;
    this.spinnerOn = true;
    this.checkFolderYear(formData);
  }

  fileUpload(url, formdata, ticket) {
    let myAuth = ticket;
    let httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: myAuth
      })
    };
    console.log('http call ! :');
    this.http.post(url, formdata, httpOptions).subscribe(
      data => {
        console.log('success'+data);
        this.resetAll();
      },
      error => {
        console.log(error);
        this.resetAll();
        alert('something bad happened');
      }
    );
  }

  checkFolderYear(formData: FormData){
    var year;
    this.nodesApi.getNodeChildren(this.PARENT_NODE).subscribe(
      data => {
        this.children = data.list.entries;
        for(let id=0; id < this.children.length; id++){
          year = this.children[id].entry;
          if (year.name == formData.get('year')){ //year.type === "fld:year" //si le dossier année actuel existe déjà
            this.PARENT_NODE = year.id;//on change l'id du dossier dans laquelle le doc doit être enregistré
            this.yearFolderExist = true;
            this.checkFolderMonth(formData);//on parcourt la liste des dossiers mois
            //A chaque nouvel appel, on doit continuer dedans sinon les appels sont exécutés après
          }
        }
          if (this.yearFolderExist == false){
            let formDataFolder = [
              {
                'name': formData.get('year'),
                'nodeType':'cm:folder'
              }
            ]
            var urlCreateFolder = this.constructUrl();
            let myAuth = this.auth.getTicketEcmBase64();
            let httpOptions = {
              headers: new HttpHeaders({
                Accept: 'application/json',
                Authorization: myAuth
              })
            };
            this.http.post(urlCreateFolder, formDataFolder, httpOptions).subscribe(
              node => {
                console.log("folder created "+node['entry'].id);
                this.PARENT_NODE = node['entry'].id;
                this.resetAll();
                this.yearFolderExist = true;
                this.checkFolderMonth(formData);
              },
              error => {
                console.log("An error is occured while folder's creation" + error);
              }
            );
            console.log('le dossier annee n existe pas' + this.PARENT_NODE);
          }
      },
      error => {
        console.log("error node year "+error);
      });
  }

  checkFolderMonth(formData: FormData){
    var month;
    this.nodesApi.getNodeChildren(this.PARENT_NODE).subscribe(
      data => {
        this.children = data.list.entries;
        for(let id=0; id < this.children.length; id++){
          month = this.children[id].entry;
          if (month.name == formData.get('month')){ //month.type === "fld:month" //si le dossier mois actuel existe déjà
            this.PARENT_NODE = month.id; //on change l'id du dossier dans laquelle le doc doit être enregistré
            this.monthFolderExist = true;
          }
        }
        if (this.monthFolderExist == false){//si le dossier année n'existe pas
          this.monthFolderExist = true;//on passe le boolean a true pour que le doc puisse etre upload dedans
          let formDataFolder = [ //creation du form folder
            {
              'name': formData.get('month'),
              'nodeType':'cm:folder'
            }
          ]
          var urlCreateFolder = this.constructUrl();
          this.createFolderMonthUpload(urlCreateFolder, formDataFolder, this.auth.getTicketEcmBase64(), formData);
          console.log('le dossier mois nexiste pas');
        }
        else { 
          console.log('le dossier mois existe');
          var url = this.constructUrl();
          this.fileUpload(url, formData, this.auth.getTicketEcmBase64());
        }
      },
      error => {
        console.log("error node month "+error);
      });
  }

  createFolderMonthUpload(url, formdatafolder, ticket, formData) {
    let myAuth = ticket
    let httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: myAuth
      })
    };
    this.http.post(url, formdatafolder, httpOptions).subscribe(
      node => {
        console.log("folder created "+node['entry'].id);
        this.PARENT_NODE = node['entry'].id;
        this.resetAll();
        var url = this.constructUrl();
        this.fileUpload(url, formData, this.auth.getTicketEcmBase64());
      },
      error => {
        console.log("An error is occured while folder's creation" + error);
      }
    );
  }

  
  constructUrl(){
    var url ='http://'+this.LOCALHOST+':8080/alfresco/api/-default-/public/alfresco/versions/1/nodes/'+this.PARENT_NODE+'/children';
    return url;
  }

  onSelect(event: Node[]){
    if (event[0]!=null){
      this.PARENT_NODE = event[0].id;
    }
    return this.PARENT_NODE;
  }

  resetAll() {
    this.ngOnInit();
  }
}